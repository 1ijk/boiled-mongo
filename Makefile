setup:
	@go get github.com/ncw/gotemplate/...
	@go mod tidy
	@go generate gitlab.com/1ijk/boiled-mongo/example

test:
	@docker run -d --name exampledb --rm -p 27017:27017 mongo &>/dev/null
	@go run ./example
	@docker stop exampledb &>/dev/null