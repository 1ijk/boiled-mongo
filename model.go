package boiled_mongo

import (
	"context"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"reflect"
	"time"
)

// template type Record(TValue)
type TValue struct{}

type Record struct {
	UUID       uuid.UUID
	CreatedAt  time.Time
	UpdatedAt  time.Time
	Attributes TValue
}

func Build(attributes TValue) *Record {
	return &Record{
		UUID:       uuid.New(),
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Attributes: attributes,
	}
}

func Create(ctx context.Context, db *mongo.Database, attributes TValue) (*Record, error) {
	m := Build(attributes)

	err := m.Save(ctx, db)

	return m, err
}

func Find(ctx context.Context, db *mongo.Database, uuid uuid.UUID) (*Record, error) {
	var m Record

	err := m.Collection(db).FindOne(ctx, bson.M{"uuid": uuid}).Decode(m)

	return &m, err
}

func SearchFor(ctx context.Context, db *mongo.Database, attributes TValue) (*Record, error) {
	var m Record

	err := m.Collection(db).FindOne(ctx, attributes).Decode(m)

	return &m, err
}

func (m *Record) CollectionName() string {
	return reflect.TypeOf(m.Attributes).String()
}

func (m *Record) Collection(db *mongo.Database) *mongo.Collection {
	return db.Collection(m.CollectionName())
}

func (m *Record) Save(ctx context.Context, db *mongo.Database) error {
	_, err := m.Collection(db).UpdateOne(
		ctx,
		bson.M{"uuid": m.UUID},
		bson.M{"$set": m},
		options.Update().SetUpsert(true),
	)

	return err
}

func (m *Record) Update(ctx context.Context, db *mongo.Database) error {
	m.UpdatedAt = time.Now()
	return m.Save(ctx, db)
}
