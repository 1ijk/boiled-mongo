package main

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

//go:generate gotemplate "gitlab.com/1ijk/boiled-mongo" MessageRecord(*Message)
type Message struct {
	Title string
	Body  string
}

func main() {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://0.0.0.0:27017"))
	if err != nil {
		log.Fatalln(err)
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(ctx)

	db := client.Database("example")

	message := BuildMessageRecord(&Message{
		Title: "Fancy",
		Body:  "look at that!",
	})

	if err := message.Save(context.Background(), db); err != nil {
		log.Fatalln(err)
	}

	log.Println(message)
}
